var express = require('express');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var jsonObj = {
    members: []
};

app.use(express.static('public'));
app.use('/scripts',express.static('node_modules/'));


app.get('/', function(req, res){

  res.sendFile(__dirname +'/public/index.html');
});

app.get('/:name', function(req, res){

  res.sendFile(__dirname +'/public/index.html');

});



io.on('connection', function(socket){

  var handshakeData = socket.request;


  var user = handshakeData._query['user'];

  console.log(user + ' has connected ');

  addUser(user, socket.id);

  //socket.emit('news', { hello: 'world' });

  socket.on('disconnect', function(){
    console.log(user + ' disconnected ');

  });

  socket.on('message', function(data) {


    console.log(data.from + ' says to ' + data.to +':'+ data.message);

    var indx = findUser(data.to);
    var id = jsonObj.members[indx].id;

    io.to(id).emit('message', data);

  });

  socket.on('typing', function(data) {

    io.to(data.to).emit('typing', data.from);

  });

});

http.listen(3000, function(){
  console.log('listen on localhost:3000');
});


function addUser(user, id){

  if (jsonObj.members.length == 0) {

    jsonObj.members.push({name:user, id : id });


  }else{

    var indx = findUser(user);
    if (indx != null) {
      jsonObj.members[indx].id = id;
    }else{
      jsonObj.members.push({name:user, id : id });
    }

  }

}

function findUser(user){
  for( var i = 0; i < jsonObj.members.length ; i++){

      if (jsonObj.members[i].name == user ) {

        return i;
      }
  }

  return null;
}

function getIdUser(user){

  var id;

  for (var i = 0; i < jsonObj.members.length; i++) {

    if (jsonObj.members.length > 0) {
      if (jsonObj.members[i].name == user) {
        id = jsonObj.members[i].id;
      }

    }

  }

  return id;
}
